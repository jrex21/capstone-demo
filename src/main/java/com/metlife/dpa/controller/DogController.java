package com.metlife.dpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metlife.dpa.model.Dog;

@Controller
public class DogController {

	@RequestMapping(value="/dog")
	public String sayHello(Model model){
		model.addAttribute("dog", "Doge");
		return "dog";
	}
	
	@RequestMapping(value="/dog", method=RequestMethod.GET)
    public String dogForm(Model model) {
        model.addAttribute("formDog", new Dog());
        return "dog";
    }

    @RequestMapping(value="/dog", method=RequestMethod.POST)
    public String dogSubmit(@ModelAttribute Dog dog, Model model) {
    	System.out.println(dog.getName() + " " + dog.getAge() + " " + dog.getBreed());
        model.addAttribute("name", dog);
        model.addAttribute("breed", dog);
        model.addAttribute("age", dog);
        return "dogresult";
    }
}
