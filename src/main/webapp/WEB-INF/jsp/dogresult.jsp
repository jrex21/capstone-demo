<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
<title>Dog Results</title>
</head>
<body style="background-color: lightblue">
	<h2 style="color: white; padding-left:10px">Congratulations!</h2>
	<h3 style="color: white; padding-left:10px">You are adopting a ${dog.age } year old ${dog.breed } named ${dog.name }</h3>
	
	<div class="container" style="background-color: white">
   	 <div class="row">
		<table class="table table-striped">
			<tr>
				<th>Name</th>
				<th>Breed</th>
				<th>Age</th>
			</tr>
			<tr>
				<td>${dog.name }</td>
    			<td>${dog.breed }</td> 
    			<td>${dog.age }</td>
			</tr>
		</table>
	 </div>
	</div>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</body>
</html>