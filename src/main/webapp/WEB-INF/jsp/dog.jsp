<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
	<title>Dog Page</title>
</head>
<body style="background-color: lightblue">
<form:form commandName="formDog">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3" style="background-color: white; border-width: 2px; border-color: green; border-style: solid; border-radius: 5px; padding: 20px; margin-top: 15px">
		<form role="form">
			<h2 style="text-align:center">Dog Information</h2>
  			<div class="form-group">
    			<label for="name">Name</label>
    			<form:input path="name" type="text" class="form-control" id="dogname" placeholder="Name" />
  			</div>
  			<div class="form-group">
    			<label for="dogbreed">Breed</label>
    			<form:input path="breed" type="text" class="form-control" id="dogbreed" placeholder="Breed" />
  			</div>
  			<div class="form-group">
    			<label for="dogage">Age</label>
    			<form:input path="age" type="text" class="form-control" id="dogage" placeholder="Age" />
  			</div>
  		</form>
  		<button type="submit" class="btn btn-default btn pull-right">Submit</button>
  	  </div>
	</div>
  </div>
  
</form:form>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</body>
</html>